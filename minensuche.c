#include <ncurses.h>
#include <stdlib.h>

/* VF MINES
 * 00000000 
 *
 * V = visible
 * F = flag
 * Mines = number of mines near by
 *      0 = no mines
 *      9 = mine
 */

void generate(int mines, int *map, int width, int height)
{
    int x,y;

    for (int i = 0; i < mines; i++)
    {
        x = random() % width;
        y = random() % height;
        map[ y * width + x ] = 9;

        for (int i = -1; i < 2; i++)
        {
            for (int ii = -1; ii < 2; ii++)
            {
                if ( x < 0 || x >= width || y < 0 || y >= height ) continue;
                if ( map[ (y+i) * width + x + ii ] == 9) continue;
                map[ (y+i) * width + x + ii ]++;
            }
        }
    }
}

void render(int *map, int width, int height)
{
    for (int i = 0; i < width * height; i++)
    {
        int x = i % width;
        int y = i / width;
        int field = map[i] & ~(3<<6);
        bool visible = (map[i] & (1<<7)) == (1<<7);
        bool masked = (map[i] & (1<<6)) == (1<<6);
        attron(COLOR_PAIR(9));
        if (visible)
        {
            attron(COLOR_PAIR(1));
            if (field == 9)  mvprintw( y,x, "B");
            else if (field == 0)  mvprintw( y,x, "%c", '_');
            else
            {
                attron(COLOR_PAIR(field));
                mvprintw( y,x, "%d", field);
            }
        }
        else if ( masked ) mvprintw(y,x, "F");
        else mvprintw(y,x, " ");
    }
}

void reveal(int *map, int x, int y, int width, int height, bool user)
{
    // border check
    if ( x < 0 || x >= width || y < 0 || y >= height ) return;
    // already revealed
    if ( (map[y * width + x] & (1<<7)) == (1<<7) ) return;
    // allow users to reveal bombs but not auto reveal
    if ( (map[y * width + x] & ~(3 << 6)) == 9 && !user ) return;
    map[y * width + x] |= 1 << 7;

    // only auto reveal field near empty flieds
    if ( (map[y * width + x] & ~(3 << 6)) > 0 ) return;
    for (int i = -1; i < 2; i++)
    {
        for (int ii = -1; ii < 2; ii++)
        {
            reveal(map, x+i, y+ii, width, height, 0);
        }
    }
    // reveal(map, x+1, y, width, height, 0);
    // reveal(map, x-1, y, width, height, 0);
    // reveal(map, x, y+1, width, height, 0);
    // reveal(map, x, y-1, width, height, 0);
}

void flag(int *map, int x, int y, int width, int height)
{
    map[y * width + x] ^= 1 << 6;
}

int main()
{
    WINDOW *stdwin = initscr();
    int width = stdwin->_maxx;
    int height = stdwin->_maxy;
    MEVENT event;
    int c;
    int *map = (int*)malloc( width * height * sizeof(int) );

    start_color();

    init_pair(1, COLOR_BLUE, COLOR_WHITE);
    init_pair(2, COLOR_GREEN, COLOR_WHITE);
    init_pair(3, COLOR_RED, COLOR_WHITE);
    init_pair(4, COLOR_MAGENTA, COLOR_WHITE);
    init_pair(5, COLOR_BLACK, COLOR_WHITE);
    init_pair(6, COLOR_BLACK, COLOR_WHITE);
    init_pair(7, COLOR_BLACK, COLOR_WHITE);
    init_pair(8, COLOR_BLACK, COLOR_WHITE);
    init_pair(9, COLOR_WHITE, COLOR_BLACK);

    //raw();
    keypad(stdscr, TRUE);
    clear();
    noecho();
    cbreak();	
    mousemask(ALL_MOUSE_EVENTS | REPORT_MOUSE_POSITION, NULL);

    // create mines
    generate(300, map, width, height);
    render(map, width, height);

    //printw("Hello World !!!");
    while (1) 
    {
        c = getch();
        if ( c == KEY_MOUSE && getmouse(&event) == OK )
        {
            if(event.bstate & BUTTON1_PRESSED) 
                reveal(map, event.x, event.y, width, height, 1);
            if(event.bstate & BUTTON3_PRESSED) 
                flag(map, event.x, event.y, width, height);

            render(map, width, height);
        }
        refresh();
    }
    endwin();
    free(map);
    return 0;
}
